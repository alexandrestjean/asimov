function testStructure = unpackTestData(testName)
%% Unpack test data function
% Load
testStructure = load(testName);

% Name self
testStructure.Name = testName;

% Store time
testStructure.time              = testStructure.data{1}.Values.Time;

% Store data
for i = 1:1:testStructure.data.numElements
    switch testStructure.data{i}.Name
        case 'torqueCMD'
            testStructure.torqueCMD                 = permute(testStructure.data{i}.Values.Data, [3 1 2]);
        case 'cartesianPosition'
            testStructure.cartesianPosition         = testStructure.data{i}.Values.Data;
        case 'cartesianVelocity'
            testStructure.cartesianVelocity         = testStructure.data{i}.Values.Data;
        case 'wrenchError'
            testStructure.wrenchError               = permute(testStructure.data{i}.Values.Data, [3 1 2]);
        case 'wrenchDesired'
            testStructure.wrenchDesired             = testStructure.data{i}.Values.Data;
        case 'idealCartesianPosition'
            testStructure.idealCartesianPosition    = permute(testStructure.data{i}.Values.Data, [3 1 2]);
        case 'cartesianWrench'
            testStructure.cartesianWrench           = permute(testStructure.data{i}.Values.Data, [3 1 2]);          
    end
end

% Calculate data
testStructure.cartesianForceNorm = vecnorm(testStructure.cartesianWrench(:,1:3)')';
testStructure.forceDesiredNorm = vecnorm(testStructure.wrenchDesired(:,1:3)')';
testStructure.forceError = (testStructure.wrenchDesired(:,1:3) - testStructure.cartesianWrench(:,1:3));
testStructure.forceErrorNorm = vecnorm(testStructure.forceError')';

% End of function
end