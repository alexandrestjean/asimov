function [frequency, gain, phase] = AnalyseFFT(OUTPUT, INPUT, Time, fe, fmin, fmax, labelYout, labelYin)
%% INITIALIZE AND FILE SELECTION :
DrawFFT = false;
DrawRawSignal = false;

t_init = 1/fe;                   % Temps � partir duquel les donn�es sont r�colt�es (s)
t_end = Time(end);

%% SELECTION signals to analyse
Entree = INPUT; %(t_init*fe:floor(t_end*fe));               % Signal d'entr�e
Sortie = OUTPUT; %(t_init*fe:floor(t_end*fe));            % Signal de sortie
% labelYin  = 'INPUT';
% labelYout = 'OUTPUT';

transferFunctionStr = strcat('( ',labelYout,' ) / ( ',labelYin,' )');

%% CALCUL fonctions de transfert
% Fen�trage
overlap    = 75;              	% En poucentage
Nfft       = 2^14;               % Nombre d'�chantillons dans 1 fen�tre
noverlap   = (overlap/100)*Nfft;
df         = fe/Nfft;
window     = hanning(Nfft);     % Fenetre Hanning
% window = rectwin(Nfft);     % Fenetre Rectangulaire

% Autospectres
[G_Entree_Entree, Freq_Entree] = pwelch(Entree, window, noverlap, Nfft, fe);      % A^2/Hz -- Autospectre du signal commande courant entree clutch
[G_Sortie_Sortie, Freq_Sortie] = pwelch(Sortie, window, noverlap, Nfft, fe);      % A^2/Hz -- Autospectre du signal courant entree clutch

% Interspectres
[G_Sortie_Entree, Freq_Sortie_Entree]  = cpsd(Entree, Sortie, window, noverlap, Nfft, fe);

% H2 Entree - Sortie
H2_Sortie_Entree     = G_Sortie_Sortie./G_Sortie_Entree;
mod_H2_Sortie_Entree = abs(H2_Sortie_Entree);
ang_H2_Sortie_Entree = unwrap(angle(H2_Sortie_Entree))*(180/pi);
% ang_H2_Sortie_Entree = (angle(H2_Sortie_Entree))*(180/pi);

% Limites
LimDB = -3*ones(length(mod_H2_Sortie_Entree(3:end)),1);
LimPhase = -180*ones(length(mod_H2_Sortie_Entree(3:end)),1);

%% PLOT signaux bruts
if(DrawRawSignal)
    Fig1 = figure('name',strcat(dataFileName,'_',fileName,'_RawData'));
    set(Fig1, 'Position', [700 50 600 600])
    % yyaxis left
    plot(time, Entree)
    set(gca,'FontSize',15)
    ylabel(strcat(labelYin,' ',UnitIn),'FontName','Times','FontSize',17);
    % yyaxis right
    plot(time,Sortie); hold on
    set(gca,'FontSize',15)
    xlabel('Time (secs)','FontName','Times','FontSize',17);
    ylabel(strcat(labelYout,' ',UnitOut),'FontName','Times','FontSize',17);
    set(gcf,'Color','W')
end

%% CALCUL/PLOT FFT
if(DrawFFT)
    % CALCUL
    x = Entree;
    y = Sortie;
    Length = numel(x);                                   % Length Of Signal
    Fn = fe/2;                                      % Nyquist Frequency
    FTx = fft(x)/Length;
    FTy = fft(y)/Length;
    Fv = linspace(0, 1, fix(Length/2)+1)*Fn;             % Frequency Vector
    Iv = 1:length(Fv);                              % Index Vector
    TFyx = FTy./FTx;
    
    % PLOT
    Fig2 = figure('name',strcat(dataFileName,'_',fileName,'_BodePlot'));
    set(Fig2, 'Position', [000 50 650 600])
    
    subplot(2,1,1)
    forInitialValue = 0*20*log10(abs(mean(TFyx(1:100))));
    semilogx(Fv, 20*log10(abs(TFyx(Iv))) - forInitialValue);
    hold on;
    
    subplot(2,1,2)
    semilogx(Fv, (angle(TFyx(Iv))*180/pi));
    hold on;
    
    all_handles = findobj(Fig2,'type','axes','tag','');
    linkaxes(all_handles,'x');
end

%% PLOT fonctions de transfert
isDCGainRemoved = true;
frequency = Freq_Sortie_Entree(1:end);
if(isDCGainRemoved)
    gain = 20*log10(mod_H2_Sortie_Entree(1:end)) - mean(20*log10(mod_H2_Sortie_Entree(1:10)));
else
    gain = 20*log10(mod_H2_Sortie_Entree(1:end));
end
phase = ang_H2_Sortie_Entree(1:end);

% figure(101);
subplot(2,1,1);
semilogx(frequency,gain,'-','linewidth',2);
hold on;
h = semilogx(linspace(0, fmax, length(LimDB)), LimDB, 'r--');
h.Annotation.LegendInformation.IconDisplayStyle = 'off';
% % h = semilogx(linspace(0, fmax, length(LimDB)), -LimDB, 'r--');
% h.Annotation.LegendInformation.IconDisplayStyle = 'off';
hold on
sgtitle(transferFunctionStr)
set(gca,'FontSize',15)
xlim([0.5 fmax])
% ylim([-15 5])
grid on;
hold on
% h = plot([5 12], [-3 -6], '-og');
% h.Annotation.LegendInformation.IconDisplayStyle = 'off';
xlabel('{\itf} (Hz)','FontName','Times','FontSize',17);
ylabel('Gain (dB) ','FontName','Times','FontSize',17);
% legend('-3 dB','Location','best');

subplot(2,1,2);
semilogx(frequency,phase,'-','linewidth',2); 
hold on;
h = semilogx(linspace(0, fmax, length(LimPhase)),LimPhase,'r--');
h.Annotation.LegendInformation.IconDisplayStyle = 'off';
hold on
% plot(Freq_Sortie_Entree(3:end),LimPhase,'r--');
set(gca,'FontSize',15)
xlim([0.5 fmax])
% ylim([-200 30])
grid on;
hold on
% h= plot([1], [-12], '-og');
% h.Annotation.LegendInformation.IconDisplayStyle = 'off';
xlabel('{\itf} (Hz)','FontName','Times','FontSize',16);
ylabel('Phase ( {\circ})','FontName','Times','FontSize',16);
% legend('-180 degrees','Location','best');

set(gcf,'color','white')

end