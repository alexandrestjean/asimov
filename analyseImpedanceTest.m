%% Asimov 6 DOF - Analyse impedance
clear all, close all, clc

%% Run robot init script (if necessary)
% Keep this section in first to avoid clearing variables
run('init.m')
tic

%% Analysis parameters
isIndividualTestPlotted = false;
nAxis = 3;

analysisNumber = input('AnalysisType :\n[1] Cube\n[2] Quasi Static\n[3] LowImpedance\n[4] HighImpedance\n--> ');
switch analysisNumber
    case 1
        analysisType = 'Cube';
    case 2
        analysisType = 'QuasiStatic';
    case 3
        analysisType = 'LowImpedance';
    case 4
        analysisType = 'HighImpedance';
end

%% Add path
addpath('.\Log')

%% Load data
% Declaration
isCubeAnalysis = false;
isQuasiStaticAnalysis = false;
isLowImpedanceAnalysis = false;
isHighImpedanceAnalysis = false;

% Select tests
switch analysisType
    
    case 'Cube'
        isCubeAnalysis = true;
        testName = {...
            'Asimov6DOF_Cube_02_K500_B_25.mat',...
            'Asimov6DOF_Cube_03_K500_B_25_withLC.mat'};
        
    case 'QuasiStatic'
        isQuasiStaticAnalysis = true;
        testName = {...
            'Asimov6DOF_QuasiStatic_LowImpedance_NoLC_R01B.mat',...
            'Asimov6DOF_QuasiStatic_LowImpedance_WithLC_R01A.mat',...
            'Asimov6DOF_QuasiStatic_HighImpedance_NoLC_R01.mat',...
            'Asimov6DOF_QuasiStatic_HighImpedance_WithLC_R01.mat'};
        isIndividualTestPlotted = true;
        
    case 'LowImpedance'
        isLowImpedanceAnalysis = true;
        testName = {...
            'Asimov6DOF_fullImpedanceAndIdealDynamics_01.mat',...
            'Asimov6DOF_fullImpedanceAndIdealDynamics_02.mat',...
            'Asimov6DOF_fullImpedanceAndIdealDynamics_03.mat',...
            'Asimov6DOF_fullImpedanceAndIdealDynamics_04.mat'};
        
    case 'HighImpedance'
        isHighImpedanceAnalysis = true;
        testName = {...
            'Asimov6DOF_fullHighImpedanceAndIdealDynamics_R01_01.mat',...
            'Asimov6DOF_fullHighImpedanceAndIdealDynamics_R01_02.mat',...
            'Asimov6DOF_fullHighImpedanceAndIdealDynamics_R01_03.mat',...
            'Asimov6DOF_fullHighImpedanceAndIdealDynamics_R01_04.mat',...
            'Asimov6DOF_fullHighImpedanceAndIdealDynamics_R01_05.mat'};
        
end

%% Unpack & Analyze Data
nTest = length(testName);
for i = 1:1:nTest
    test{i} = unpackTestData(testName{i});
    
    % Declare unlogged test parameters
    test{i}.referencePosition = repmat([0.65; 0.00; 0.25; 3*pi/4; 0; pi/2]',length(test{i}.cartesianPosition),1);
    test{i}.cartesianPositionError = test{i}.referencePosition - test{i}.cartesianPosition;
    test{i}.idealCartesianPositionError = test{i}.referencePosition(:,1:3) - test{i}.idealCartesianPosition;
    test{i}.isWrenchCommand = vecnorm(test{i}.wrenchDesired')' > 0;
    test{i}.startIndex = find(test{i}.isWrenchCommand,1);
    test{i}.endIndex = find(not(test{i}.isWrenchCommand(test{i}.startIndex:end)),1) + test{i}.startIndex;
    test{i}.startTime = test{i}.time(test{i}.startIndex);
    test{i}.endTime = test{i}.time(test{i}.endIndex);
    
    if(isLowImpedanceAnalysis || isHighImpedanceAnalysis)
        % Analyse transfer function
        fmin = 0;
        fmax = 10;
        labelYin = 'Force Norm';
        labelYout  = 'Displacement Norm';
        
        figure
        % Expected impedance
        [idealFrequency(:,i), idealGain(:,i), idealPhase(:,i)] = AnalyseFFT(vecnorm(test{i}.idealCartesianPositionError(test{i}.startIndex:test{i}.endIndex,1:3)')',test{i}.cartesianForceNorm(test{i}.startIndex:test{i}.endIndex,:),  test{i}.time(test{i}.startIndex:test{i}.endIndex), 1/mean(gradient(test{i}.time)), fmin, fmax, labelYout, labelYin);
        
        % Measured impedance
        [measuredFrequency(:,i), measuredGain(:,i), measuredPhase(:,i)] = AnalyseFFT(vecnorm(test{i}.cartesianPositionError(test{i}.startIndex:test{i}.endIndex,1:3)')',test{i}.cartesianForceNorm(test{i}.startIndex:test{i}.endIndex,:),  test{i}.time(test{i}.startIndex:test{i}.endIndex), 1/mean(gradient(test{i}.time)), fmin, fmax, labelYout, labelYin);
        
        % Difference
        differenceGain(:,i) = idealGain(:,i) - measuredGain(:,i);
        differencePhase(:,i) = -(idealPhase(:,i) - measuredPhase(:,i));
        subplot(2,1,1)
        plot(idealFrequency(:,i), differenceGain(:,i),'--k')
        xlim([0.75 fmax])
        ylim([-20 20])
        subplot(2,1,2)
        plot(idealFrequency(:,i), differencePhase(:,i),'--k')
        ylim([-200 200])
        
        % Legend
        legend('Ideal spring-damper','Measured','Difference')
    end
end

if(not(isIndividualTestPlotted))
    close all
end

%% Mean of transfer functions
if(isLowImpedanceAnalysis || isHighImpedanceAnalysis)
    meanIdealGain = mean(idealGain,2);
    meanIdealPhase = mean(idealPhase,2);
    meanMeasuredGain = mean(measuredGain,2);
    meanMeasuredPhase = mean(measuredPhase,2);
    
    PlotMeanTf(idealFrequency(:,1), meanIdealGain, meanIdealPhase, meanMeasuredGain, meanMeasuredPhase, fmax);
    set(gcf, 'position', [1,41,1366,651]);
end

%% Plots
% Force plots (desired, measured, error)
if(isIndividualTestPlotted)
    timeLimit = [0 60];
    forceLimit = [-75 75];
    titleStr = {'X','Y','Z'};
    
    for i = 1:1:nTest
        plotNumber = 0;
        figure;
        sgtitle(test{i}.Name(24:end-4),'Interpreter','none','FontName','Times New Roman')
        for j = 1:1:nAxis
            plotNumber = plotNumber + 1;
            ax(i,j) = subplot(2,nAxis,plotNumber);
            plot(test{i}.time(test{i}.startIndex:test{i}.endIndex,:) - test{i}.startTime, test{i}.wrenchDesired(test{i}.startIndex:test{i}.endIndex,j),'Color',[0 0 0],'LineWidth',2)
            hold on
            plot(test{i}.time(test{i}.startIndex:test{i}.endIndex,:)- test{i}.startTime, test{i}.cartesianWrench(test{i}.startIndex:test{i}.endIndex,j),'Color',[130 130 130]/255,'LineWidth',2)
            hold on
            grid on
            xlim(timeLimit)
            ylim(forceLimit)
            xlabel('Time (s)','FontName','Times New Roman')
            ylabel('Force (N)','FontName','Times New Roman')
            title(titleStr{j})
            legend('Desired','Measured','Location','SouthEast','FontName','Times New Roman')
        end
        
        for j = 1:1:nAxis
            plotNumber = plotNumber + 1;
            ax(i,j) = subplot(2,nAxis,plotNumber);
            plot(test{i}.time(test{i}.startIndex:test{i}.endIndex,:) - test{i}.startTime, test{i}.forceError(test{i}.startIndex:test{i}.endIndex,j),'-k')
            grid on
            xlim(timeLimit)
            ylim(forceLimit)
            xlabel('Time (s)','FontName','Times New Roman')
            ylabel('Force error (N)','FontName','Times New Roman')
        end
        
        linkaxes(ax, 'x')
        clear ax
        set(gcf, 'position', [1,41,1366,651]);
    end
end

%% Virtual cube plots
if(isCubeAnalysis)
    % Plot parameters
    markerSize = 1;
    boxWidth = 0.1;
    xForceLimit = 0.65 + 0.75*boxWidth*[-1 1];
    yForceLimit = 0.00 + 0.75*boxWidth*[-1 1];
    zForceLimit = 0.25 + 0.75*boxWidth*[-1 1];
    forceErrorLimits = [0 25];
    
    % Plots
    figure
    sgtitle('User back driving force error','FontName','Times New Roman')
    for i = 1:1:nTest
        ax(i) = subplot(1,nTest,i);
        scatter3(test{i}.cartesianPosition(:,1),test{i}.cartesianPosition(:,2),test{i}.cartesianPosition(:,3),markerSize,test{i}.forceErrorNorm)
        axis equal
        xlim(xForceLimit)
        ylim(yForceLimit)
        zlim(zForceLimit)
        xlabel('X (m)','FontName','Times New Roman')
        ylabel('Y (m)','FontName','Times New Roman')
        zlabel('Z (m)','FontName','Times New Roman')
        cbar = colorbar;
        cbar.Label.String = 'Force error norm (N)';
        cbar.Label.FontName = 'Times New Roman';
        cbar.Label.FontSize = 14;
        caxis(forceErrorLimits);
        title(test{i}.Name,'Interpreter','none','FontName','Times New Roman')
    end
    set(gcf, 'position', [1,41,1366,651]);
    Link = linkprop(ax,{'CameraUpVector', 'CameraPosition', 'CameraTarget', 'XLim', 'YLim', 'ZLim'});
    setappdata(gcf, 'StoreTheLink', Link);
    set(gcf, 'position', [1,41,1366,651]);

end

%% Force rendering surface
if(isQuasiStaticAnalysis || isLowImpedanceAnalysis)
    clear ax
    markerSize = 1;
    forceLimit = [-75 75];
    titleStr = {'X','Y','Z'};
    if(isLowImpedanceAnalysis)
        figure;
        sgtitle('Low Impedance')
    end
    for i = 1:1:nTest
        if(isQuasiStaticAnalysis)
        figure;
%         sgtitle(test{i}.Name(24:end-4),'Interpreter','none','FontName','Times New Roman')
        end
        for j = 1:1:nAxis
            ax(i,j) = subplot(1, nAxis, j);
            s2 = scatter3(test{i}.cartesianPositionError(test{i}.startIndex:test{i}.endIndex,j), test{i}.cartesianVelocity(test{i}.startIndex:test{i}.endIndex,j), test{i}.wrenchDesired(test{i}.startIndex:test{i}.endIndex,j),markerSize);
            s2.MarkerEdgeColor = [0 0 0]/255;
            s2.MarkerFaceColor = s2.MarkerEdgeColor;
            hold on
            s1 = scatter3(test{i}.cartesianPositionError(test{i}.startIndex:test{i}.endIndex,j), test{i}.cartesianVelocity(test{i}.startIndex:test{i}.endIndex,j), test{i}.cartesianWrench(test{i}.startIndex:test{i}.endIndex,j),markerSize);
            s1.MarkerEdgeColor = [130 130 130]/255;
            s1.MarkerFaceColor = s1.MarkerEdgeColor;
            hold on
            grid on
            zlim(forceLimit)
            xlabel('Position error (m)','FontName','Times New Roman')
            ylabel('Velocity (m/s)','FontName','Times New Roman')
            zlabel('Force (N)','FontName','Times New Roman')
%             title(titleStr{j})
            legend('Desired','Measured','Location','northwest')
            view([0 0])
            xlim([-0.03 0.03])
            zlim([-40 40])
        end
        Link = linkprop(ax(i,:),{'CameraUpVector', 'CameraPosition', 'CameraTarget', 'XLim', 'YLim', 'ZLim'});
        setappdata(gcf, 'StoreTheLink', Link);
        set(gcf, 'position', [1,41,1366,651]);
    end
end

%% --- END OF MAIN SCRIPT
toc

%% --- EXTRA FUNCTION ---

%% Plot mean of impedance transfer functions
function [] = PlotMeanTf(frequency, meanIdealGain, meanIdealPhase, meanMeasuredGain, meanMeasuredPhase, fmax)
isDifferencePloted = false;
LimDB = [-3 -3];
LimPhase = -135;
fminPlot = 0.75;

figure
subplot(2,1,1);
semilogx(frequency,meanIdealGain,'-','linewidth',2);
hold on;
semilogx(frequency,meanMeasuredGain,'-','linewidth',2);
hold on;
if(isDifferencePloted)
    semilogx(frequency,-(meanIdealGain-meanMeasuredGain),'-','linewidth',2);
    hold on
end
h = semilogx(linspace(0, fmax, length(LimDB)), LimDB, 'r--');
h.Annotation.LegendInformation.IconDisplayStyle = 'off';
hold on
sgtitle('Displacement / Force')
set(gca,'FontSize',15)
xlim([fminPlot fmax])
grid on;
hold on
xlabel('{\itf} (Hz)','FontName','Times','FontSize',17);
ylabel('Gain (dB) ','FontName','Times','FontSize',17);

subplot(2,1,2);
semilogx(frequency,meanIdealPhase,'-','linewidth',2);
hold on;
semilogx(frequency,meanMeasuredPhase,'-','linewidth',2);
hold on;
if(isDifferencePloted)
    semilogx(frequency,-(meanIdealPhase-meanMeasuredPhase),'-','linewidth',2);
    hold on;
end
h = semilogx(linspace(0, fmax, length(LimPhase)),LimPhase,'r--');
h.Annotation.LegendInformation.IconDisplayStyle = 'off';
hold on
set(gca,'FontSize',15)
xlim([fminPlot fmax])
grid on;
hold on
xlabel('{\itf} (Hz)','FontName','Times','FontSize',16);
ylabel('Phase ( {\circ})','FontName','Times','FontSize',16);

set(gcf,'color','white')

legend('Ideal mass-damper with real force input','Measured','Difference')
end