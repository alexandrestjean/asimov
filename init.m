%% Démonstration pour Zimmer avec 1 seul bras du projet Asimov
% Script pour créer les valeurs des diverses matrices
clear all; close all; clc;

%% Execution parameters
fs = 12500;  % Sample frequency, Hz
Ts = 1/fs  % Sample time, sec

%% Déclaration pour contrôleur en force
% Gain d'asservissement
% kp = 0.1*eye(3);
% ki = 0*eye(3);
% kd = 0*eye(3);

% Déclaration de la matrice des biais sur la commande de couple
TorqueBias = eye(3);

%% Déclaration des paramètres géométriques et inertiels
% Bras
L1 = .525;  % m
L2 = .375;  % m
mj = 0.832; % masse joint coude kg
m1m= .915;  % masse membrure bras
m2m= .576;  % masse membrure avant bras

% Payload
mo=0;      %mass object réel
mo_ctrl=mo; %mass object contrôleur

%% Déclaration des paramètres des capteurs
resenc=16394; % résolution des encodeurs

% Filtre pour vitesse angulaire
fc = 10;    % Hz
filterOrder = 1;
[b_angularVelocity,a_angularVelocity] = butter(filterOrder,fc/(fs/2),'low');


% [b_lcfilter,a_lcfilter] = butter(2,100/500);
% [b_epfilter,a_epfilter] = butter(2,50/500);
% [b_accelfilter, a_accelfilter] = butter(4, 30/500);
% [a_cmdfilter, b_cmdfilter] = butter(2,50/500);
% [b_accelfiltertest,a_accelfiltertest] = butter(4,15/500);
% %% Filtre passe bas
% N=3;
% w=0.05;
% %[num_filt,den_filt] = butter(N,w,'low');
% num_filt=[0.0004    0.0012    0.0012    0.0004];
% den_filt=[1.0000   -2.6862    2.4197   -0.7302];

%% Misc. declaration (?)
friction=0;
tctrl=6.1;
Ib=0.83;

%% Chargement des lookup table de caractérisation des clutchs
% load lookuptable
mat = dir(['lookuptable\','*.mat']);
for q = 1:length(mat) 
    load(['lookuptable\',mat(q).name]);
end



%% -------------------------- Robot DATA -------------------------- %%
lxa     = 0;
lya     = 0;
lza     = -0.15185;

lxb     = 0;
lyb     = 0.525;
lzb     = 0;

lxc     = 0;
lyc     = 0.11;
lzc     = 0;

lxd     = 0;
lyd     = 0;
lzd     = 0.316;

lxe     = 0.04;
lye     = 0;
lze     = 0;

lxf     = 0;
lyf     = 0;
% lzf     = 0.03;
lzf     = 0.139;    % With handle installed


% Mass/Inertia
% Ia = .1;
% ma = 0;
% Ib = .1;
% mb = 0;
% Ic = .1;
% mc = 1.5;
% Id = .1;
% md = .73;
% Ie = .1;
% me = .73;
% If = .1;
% mf = .73;

Ia = .1;
ma = 0;
Ib = .1;
mb = 5.0;
Ic = .1;
mc = 0.5;
Id = .1;
md = 1.5;
Ie = .1;
me = .25;
If = .1;
mf = .2;

% Temps 

TF = 34;


Parameter =     [lxa lya lza lxb lyb lzb...
                 lxc lyc lzc lxd lyd lzd...
                 lxe lye lze lxf lyf lzf...
                 Ia  ma  Ib  mb  Ic  mc...
                 Id  md  Ie  me  If  mf...
                 TF];
