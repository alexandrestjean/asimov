% MotionGenesis file:  MGTemplateBasic.txt
% Copyright (c) 2009-2017 Motion Genesis LLC.  Only for use with MotionGenesis.
%--------------------------------------------------------------------
%   MG Parameters
SetAutoZee( ON )
%--------------------------------------------------------------------
%   Physical objects.
NewtonianFrame  N                % Newtonian reference frame
RigidBody       B1               % 1st link
RigidBody       B2               % 2nd link
RigidBody       B3               % 3rd link
RigidBody       B4               % 1st link
RigidBody       B5               % 2nd link
RigidBody       B6               % 3rd link
RigidBody 		EE				 % End-effector body
RigidFrame 		F1				 % Frame fixed to B1
RigidFrame 		F2				 % Frame fixed to B2
RigidFrame 		F3				 % Frame fixed to B3
RigidFrame 		F4				 % Frame fixed to B4
RigidFrame 		F5				 % Frame fixed to B5
RigidFrame 		F6				 % Frame fixed to B6
Point 			TCP() 			 % Tool center point
%--------------------------------------------------------------------
%   Mathematical declarations.
Variable    q1'', q2'', q3'', q4'', q5'', q6''  	% Angular acceleration and derivatives
Constant    g = 9.80665 m/s^2    					% Earth's gravitational acceleration
Constant    a1, a2, a3, a4, a5, a6 					% Member length & offset
Constant    b1, b2, b3, b4, b5, b6					% Member length & offset
Constant    B1cmx, B1cmy, B1cmz	 					% Center of mass position in member-fix frame
Constant    B2cmx, B2cmy, B2cmz	 					% Center of mass position in member-fix frame
Constant    B3cmx, B3cmy, B3cmz	 					% Center of mass position in member-fix frame
Constant    B4cmx, B4cmy, B4cmz	 					% Center of mass position in member-fix frame
Constant    B5cmx, B5cmy, B5cmz	 					% Center of mass position in member-fix frame
Constant    B6cmx, B6cmy, B6cmz	 					% Center of mass position in member-fix frame
Constant    EEcmx, EEcmy, EEcmz	 					% Center of mass position in member-fix frame
Constant    TCPx,  TCPy,  TCPz	 					% Center of mass position in member-fix frame
Constant    damp1, damp2, damp3, damp4, damp5, damp6% Damping coefficient
Specified   T1, T2, T3, T4, T5, T6
SetGeneralizedSpeeds( q1', q2', q3', q4', q5', q6')
%--------------------------------------------------------------------
%   Mass and inertia properties.
B1.SetMass( mB1 )
B2.SetMass( mB2 )
B3.SetMass( mB3 )
B4.SetMass( mB4 )
B5.SetMass( mB5 )
B6.SetMass( mB6 )
EE.SetMass( mEE )
B1.SetInertia( B1cm,  IB1xx,  IB1yy,  IB1zz )
B2.SetInertia( B2cm,  IB2xx,  IB2yy,  IB2zz )
B3.SetInertia( B3cm,  IB3xx,  IB3yy,  IB3zz )
B4.SetInertia( B4cm,  IB4xx,  IB4yy,  IB4zz )
B5.SetInertia( B5cm,  IB5xx,  IB5yy,  IB5zz )
B6.SetInertia( B6cm,  IB6xx,  IB6yy,  IB6zz )
EE.SetInertia( EEcm,  IEExx,  IEEyy,  IEEzz )
%--------------------------------------------------------------------
%   Frames - Rotational kinematics.
F1.RotateZ( N, q1 )
F2.Rotate( F1, BodyXZY, pi/2, (pi/2 + q2), 0 )
F3.RotateZ( F2, q3 )
F4.Rotate( F3, BodyXZY, pi/2, q4, 0 )
F5.Rotate( F4, BodyXZY, -pi/2, (q5 - pi/2), 0 )
F6.Rotate( F5, BodyYZX, pi/2, q6, 0 )   
%--------------------------------------------------------------------
%   Bodies - Rotational kinematics.
B1.RotateZ( F1, 0)
B2.RotateZ( F2, 0)
B3.RotateZ( F3, 0)
B4.RotateZ( F3, 0)
B5.RotateZ( F5, 0)
B6.RotateZ( F6, 0)
EE.RotateZ( F6, 0)
%--------------------------------------------------------------------
%   Frames - Translational kinematics.
F1o.Translate( No, 0> )          % Sets position, velocity, acceleration
F2o.Translate( F1o, a1*F1z> )
F3o.Translate( F2o, a2*F2x> ) 
F4o.Translate( F3o, a3*F3x> )
F5o.Translate( F4o, a4*F4z> )
F6o.Translate( F5o, a5*F5x> ) 
TCP.Translate( F6o, TCPx*F6x> + TCPy*F6y> + TCPz*F6z> )
%--------------------------------------------------------------------
%   Bodies - Translational kinematics.
B1o.Translate( F1o, 0>)
B2o.Translate( F2o, 0>) 
B3o.Translate( F3o, 0>)
B4o.Translate( F4o, 0>)
B5o.Translate( F5o, 0>) 
B6o.Translate( F6o, 0>)
EEo.Translate( TCP, 0>)
% B1cm.Translate( B1o, B1cmx*F1x> + B1cmy*F1y> + B1cmz*F1z> )
% B2cm.Translate( B2o, B2cmx*F2x> + B2cmy*F2y> + B2cmz*F2z> )
% B3cm.Translate( B3o, B3cmx*F3x> + B3cmy*F3y> + B3cmz*F3z> )
% B4cm.Translate( B4o, B4cmx*F4x> + B4cmy*F4y> + B4cmz*F4z> )
% B5cm.Translate( B5o, B5cmx*F5x> + B5cmy*F5y> + B5cmz*F5z> )
% B6cm.Translate( B6o, B6cmx*F6x> + B6cmy*F6y> + B3cmz*F6z> )
% EEcm.Translate( EEo, EEcmx*F6x> + EEcmy*F6y> + EEcmz*F6z> )
% Simplify expression with because of robot member symetry
B1cm.Translate( B1o, B1cmz*F1z> )
B2cm.Translate( B2o, B2cmx*F2x> )
B3cm.Translate( B3o, B3cmx*F3x> + B3cmy*F3y> )
B4cm.Translate( B4o, B4cmz*F4z> )
B5cm.Translate( B5o, B5cmx*F5x> )
B6cm.Translate( B6o, B6cmz*F6z> )
EEcm.Translate( EEo, EEcmx*F6x> + EEcmy*F6y> + EEcmz*F6z> )
%--------------------------------------------------------------------
%   Show rotation matrix (EE to N)
R_EE_N = Explicit(EE.GetRotationMatrix( N ))

%--------------------------------------------------------------------
%   Kinetic energy related calculation
% DoubleEK = FactorQuadratic(2*System.GetKineticEnergy(), q1', q2', q3', q4', q5', q6')

% Explicit(DoubleEK)
%--------------------------------------------------------------------
%   Add relevant contact/distance forces.
System.AddForceGravity( -g*Nz> )
Tg[1] = dot(F1z>, System.GetMomentOfForces(F1o))
Tg[2] = dot(F2z>, System(B2,B3,B4,B5,B6,EE).GetMomentOfForces(F2o))
Tg[3] = dot(F3z>, System(B3,B4,B5,B6,EE).GetMomentOfForces(F3o))
Tg[4] = dot(F4z>, System(B4,B5,B6,EE).GetMomentOfForces(F4o))
Tg[5] = dot(F5z>, System(B5,B6,EE).GetMomentOfForces(F5o))
Tg[6] = dot(F6z>, System(B6,EE).GetMomentOfForces(F6o))
%--------------------------------------------------------------------
%   Add relevant torques.
B1.AddTorque(  N, T1*F1z> )     % Law of action/reaction
B2.AddTorque( B1, T2*F2z> )     % Law of action/reaction
B3.AddTorque( B2, T3*F3z> )     % Law of action/reaction
B4.AddTorque( B3, T4*F4z> )     % Law of action/reaction
B5.AddTorque( B4, T5*F5z> )     % Law of action/reaction
B6.AddTorque( B5, T6*F6z> )     % Law of action/reaction
% B1.AddTorque( -damp1*q1'*F1z> )
% B2.AddTorque( -damp2*q2'*F2z> )
% B3.AddTorque( -damp3*q3'*F3z> )
% B4.AddTorque( -damp4*q4'*F4z> )
% B5.AddTorque( -damp5*q5'*F5z> )
% B6.AddTorque( -damp6*q6'*F6z> )
%--------------------------------------------------------------------
%   F = m*a    (translational equations of motion).
% 	N/A
%--------------------------------------------------------------------
%   M = DH/Dt  (rotational equations of motion).
% KaneDynamicEqns = System.GetDynamicsKane()
% Dynamics[1] = Dot(  System.GetDynamics(F1o),  F1z>  )
% Dynamics[2] = Dot(  System(B2,B3,B4,B5,B6,EE).GetDynamics(F2o),  F2z>  )
% Dynamics[3] = Dot(  System(B3,B4,B5,B6,EE).GetDynamics(F3o),  F3z>  )
% Dynamics[4] = Dot(  System(B4,B5,B6,EE).GetDynamics(F4o),  F4z>  )
% Dynamics[5] = Dot(  System(B5,B6,EE).GetDynamics(F5o),  F5z>  )
% Dynamics[6] = Dot(  System(B6,EE).GetDynamics(F6o),  F6z>  )
%--------------------------------------------------------------------
%   Solve linear equations for list of unknowns.
% Solve( KaneDynamicEqns,  q1'', q2'', q3'', q4'', q5'', q6'' )
% Algebraic( KaneDynamicEqns,  q1'', q2'', q3'', q4'', q5'', q6'' ) UR5_EOM.m
%--------------------------------------------------------------------
%   Jacobian
xp = dot(v_TCP_N>, Nx> )
yp = dot(v_TCP_N>, Ny> )
zp = dot(v_TCP_N>, Nz> )
wx = dot(w_F6_N>, Nx> )
wy = dot(w_F6_N>, Ny> )
wz = dot(w_F6_N>, Nz> )

J[1,1] = D(xp, q1')
J[1,2] = D(xp, q2')
J[1,3] = D(xp, q3')
J[1,4] = D(xp, q4')
J[1,5] = D(xp, q5')
J[1,6] = D(xp, q6')

J[2,1] = D(yp, q1')
J[2,2] = D(yp, q2')
J[2,3] = D(yp, q3')
J[2,4] = D(yp, q4')
J[2,5] = D(yp, q5')
J[2,6] = D(yp, q6')

J[3,1] = D(zp, q1')
J[3,2] = D(zp, q2')
J[3,3] = D(zp, q3')
J[3,4] = D(zp, q4')
J[3,5] = D(zp, q5')
J[3,6] = D(zp, q6')

J[4,1] = D(wx, q1')
J[4,2] = D(wx, q2')
J[4,3] = D(wx, q3')
J[4,4] = D(wx, q4')
J[4,5] = D(wx, q5')
J[4,6] = D(wx, q6')

J[5,1] = D(wy, q1')
J[5,2] = D(wy, q2')
J[5,3] = D(wy, q3')
J[5,4] = D(wy, q4')
J[5,5] = D(wy, q5')
J[5,6] = D(wy, q6')

J[6,1] = D(wz, q1')
J[6,2] = D(wz, q2')
J[6,3] = D(wz, q3')
J[6,4] = D(wz, q4')
J[6,5] = D(wz, q5')
J[6,6] = D(wz, q6')
Explicit(J)

%--------------------------------------------------------------------
%   Position & Orientation of Effector
xEE = Explicit(dot(p_No_EEo>, Nx> ))
yEE = Explicit(dot(p_No_EEo>, Ny> ))
zEE = Explicit(dot(p_No_EEo>, Nz> ))
% rxEE = dot(p_No_EEcm>, Nx> )
% ryEE = dot(p_No_EEcm>, Ny> )
% rzEE = dot(p_No_EEcm>, Nz> )
%--------------------------------------------------------------------
%   Point of interest (POI) matrix
% Base point (Origin)
POI[1,1] = 0
POI[1,2] = 0
POI[1,3] = 0

% Center of Joint 1
POI[2,1] = 0
POI[2,2] = 0
POI[2,3] = b1

% Center of Joint 2
POI[3,1] = dot(p_No_F2o>, Nx>)
POI[3,2] = dot(p_No_F2o>, Ny>)
POI[3,3] = dot(p_No_F2o>, Nz>)

% Center of Joint 3
POI[4,1] = dot(p_No_F3o>, Nx>)
POI[4,2] = dot(p_No_F3o>, Ny>)
POI[4,3] = dot(p_No_F3o>, Nz>)

% Offset center of Joint 3 on body 3
POI[5,1] = dot(p_No_F3o> - b3*F3z>, Nx>)
POI[5,2] = dot(p_No_F3o> - b3*F3z>, Ny>)
POI[5,3] = dot(p_No_F3o> - b3*F3z>, Nz>)

% Center of Joint 4
POI[6,1] = dot(p_No_F4o>, Nx>)
POI[6,2] = dot(p_No_F4o>, Ny>)
POI[6,3] = dot(p_No_F4o>, Nz>)

% Center of Joint 5
POI[7,1] = dot(p_No_F5o>, Nx>)
POI[7,2] = dot(p_No_F5o>, Ny>)
POI[7,3] = dot(p_No_F5o>, Nz>)

% Center of Joint 6
POI[8,1] = dot(p_No_F6o>, Nx>)
POI[8,2] = dot(p_No_F6o>, Ny>)
POI[8,3] = dot(p_No_F6o>, Nz>)

% Center of End-effector plate
POI[9,1] = dot(p_No_F7o>, Nx>)
POI[9,2] = dot(p_No_F7o>, Ny>)
POI[9,3] = dot(p_No_F7o>, Nz>)

% End-effector TCP
POI[10,1] = dot(p_No_TCP>, Nx>)
POI[10,2] = dot(p_No_TCP>, Ny>)
POI[10,3] = dot(p_No_TCP>, Nz>)

Explicit(POI)
%--------------------------------------------------------------------
%   Initial values for variables (e.g., for ODE command).
Input  q1 = 0 rad, q2 = 0 rad, q3 = 0 rad, q4 = 0 rad, q5 = 0 rad, q6 = 0 rad
Input  q1' = 0 rad/s, q2' = 0 rad/s, q3' = 0 rad/s, q4' = 0 rad/s, q5' = 0 rad/s, q6' = 0 rad/s
a1 = 0.1378
a2 = 0.425
a3 = 0.392
a4 = 0
a5 = 0
a6 = 0
b1 = 0.1625
b2 = 0
b3 = 0.1312
b4 = 0.1267
b5 = 0.0997
b6 = 0.0996
B1cmx = 0
B1cmy = 0.1625
B1cmz = 0
B2cmx = 0.2125
B2cmy = 0
B2cmz = 0
B3cmx = 0.15
B3cmy = 0
B3cmz = 0
B4cmx = 0
B4cmy = 0
B4cmz = 0.01634
B5cmx = 0
B5cmy = 0
B5cmz = 0.01634
B6cmx = 0
B6cmy = 0
B6cmz = 0
EEcmx = 0
EEcmy = 0
EEcmz = 0
TCPx = 0
TCPy = 0
TCPz = 0
mB1 = 3.761
mB2 = 8.058
mB3 = 2.846
mB4 = 1.37
mB5 = 1.3
mB6 = 0.365
mEE = 0
IB1xx = 0
IB1yy = 0
IB1zz = 0.5676
IB2xx = 0.0147
IB2yy = 0.0147
IB2zz = 0.5676
IB3xx = 0.0147
IB3yy = 0.0147
IB3zz = 0.5676
IB4xx = 0.00147
IB4yy = 0.00147
IB4zz = 0.2193
IB5xx = 0.00147
IB5yy = 0.00147
IB5zz = 0.2193
IB6xx = 0.00147
IB6yy = 0.00147
IB6zz = 0.2193
IEExx = 0
IEEyy = 0
IEEzz = 0
damp1 = 100
damp2 = 10
damp3 = 10
damp4 = 1
damp5 = 1
damp6 = 1

% --------------------------------------------------------------------
%  List output quantities (e.g., for ODE command).
% Output      t sec,  x m,  Fx Newton,  Fy Newton
OutputPlot  t sec, xEE m, yEE m, zEE m
OutputPlot  t sec,  q1 degrees,  q2 degrees,  q3 degrees, q4 degrees,  q5 degrees,  q6 degrees
OutputEncode J
OutputEncode POI
OutputEncode DoubleEK
% --------------------------------------------------------------------
  % Solve ODEs (or auto-generate MATLAB, C, Fortran, ... code).
Input  tFinal = 10 sec,  tStep = 0.02 sec,  absError = 1.0E-06
ODE()  MR3_6DOF.m    % or ODE( Zero, listOfVariablesToSolve ) Filename.m